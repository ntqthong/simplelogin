from flask import Flask, redirect, url_for, render_template
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, IntegerField
from wtforms.validators import InputRequired, Email, Length
from werkzeug.security import generate_password_hash, check_password_hash
import logging
import os

logging.basicConfig(level=logging.DEBUG)

app = Flask(__name__)
app.config["SECRET_KEY"] = "secretkey"
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:////" + os.getcwd() + "/database.db"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

bootstrap = Bootstrap(app)
db = SQLAlchemy(app)
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"

class User(UserMixin, db.Model):
	id = db.Column(db.Integer, primary_key = True)
	name= db.Column(db.String(15))
	email = db.Column(db.String(50), unique = True)
	password = db.Column(db.String(80))
	alias = db.relationship("Alias", backref = "user", lazy = True)

class Alias(db.Model):
	id = db.Column(db.Integer, primary_key = True)
	alias = db.Column(db.String(50), unique = True)
	id_user = db.Column(db.Integer, db.ForeignKey('user.id'), nullable = False)

db.create_all()
db.session.commit()

@login_manager.user_loader
def load_user(user_id):
	return User.query.get(int(user_id))

class SignUpForm(FlaskForm):
	email = StringField('email', validators=[InputRequired(), Email(message='Invalid email'), Length(max=50)])
	name = StringField('name', validators=[InputRequired(), Length(min=4, max=15)])
	password = PasswordField('password', validators=[InputRequired(), Length(min=8, max=80)])

class LoginForm(FlaskForm):
	email = StringField('email', validators=[InputRequired(), Email(message='Invalid email'), Length(max=50)])
	password = PasswordField('password', validators=[InputRequired(), Length(min=8, max=80)])
	remember = BooleanField('remember me')

class AliasForm(FlaskForm):
	alias = StringField('alias', validators=[InputRequired(), Length(max=50)])
	id_user = IntegerField('id_user')

@app.route("/home")
@app.route("/")
def home():
	return render_template("index.html")

@app.route("/signup", methods=["GET", "POST"])
def signup():
	form = SignUpForm()
	if form.validate_on_submit():
		hashed_password = generate_password_hash(form.password.data, method = "sha256")
		new_user = User(email=form.email.data, name=form.name.data, password=hashed_password)
		db.session.add(new_user)
		db.session.commit()
		return redirect(url_for('login'))
	return render_template("signup.html", form = form)

@app.route("/login", methods=["GET", "POST"])
def login():
	form = LoginForm()
	if form.validate_on_submit():
		user = User.query.filter_by(email=form.email.data).first()
		if user:
			if check_password_hash(user.password, form.password.data):
				login_user(user, remember=form.remember.data)
				return redirect(url_for('dashboard'))
		return '<h1>Invalid email or password</h1>'
	return render_template("login.html", form = form)

@app.route("/dashboard", methods=["GET", "POST"])
@login_required
def dashboard():
	aliases = Alias.query.filter_by(id_user=current_user.id).all()
	form = AliasForm()
	if form.validate_on_submit():
		user_id = current_user.id
		new_alias = Alias(alias=form.alias.data, id_user=user_id)
		db.session.add(new_alias)
		db.session.commit()
	return render_template("dashboard.html", form = form, name=current_user.email, aliases = aliases)

@app.route("/logout")
@login_required
def logout():
	logout_user()
	return redirect(url_for('login'))

if __name__ == "__main__":
    app.run(debug = True)