# Simple Login

Project SimpleLogin

## Content

- back-end: python flask
- front-end: html, css

Front and back communicate using Flask. Using HTML/CSS template to design interface

## Installation

- Install Python: https://www.python.org/downloads/
- Install pip: https://pip.pypa.io/en/stable/installing/

### Requirements

- Install Flask:
	```
	pip install Flask
	```
- Install flask_bootstrap:
	```
	pip install flask_bootstrap
	```
- Install flask_sqlalchemy:
	```
	pip install flask_sqlalchemy
	```
- Install flask_login:
	```
	pip install flask_login
	```
- Install flask_wtf:
	```
	pip install flask_wtf
	```
- Install email-validator (if needed):
	```
	pip install email-validator
	```

### Run backend

1. At the root of the project, execute:
	```
	python "app.py"
	```

### Run frontend

copy this address http://127.0.0.1:5000/ to the navigator

## Features

Homepage, Signup page, Login page, Dashboard page

- Signup: create account
- Login: using created account to login
- Dashboard: create alias, delete existing alias, logout
